package winium.desktop.winiumTest;

import org.openqa.selenium.By;
import org.openqa.selenium.Keys;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.chrome.ChromeDriver;
import org.openqa.selenium.interactions.Actions;
import org.sikuli.script.FindFailed;
import org.sikuli.script.Key;
import org.sikuli.script.KeyModifier;
import org.sikuli.script.Screen;
import org.testng.annotations.Test;

public class WiniumAutomation {

	@Test

	public void openAPP() throws InterruptedException {
		System.setProperty("webdriver.chrome.driver", "chromedriver.exe");

		WebDriver driver = new ChromeDriver();
		driver.get("https://workspace01.t-mobile.com/Citrix/CDE_Internal_StoreWeb/");
		waitForElement();

		driver.findElement(By.id("username")).sendKeys("gsm1900\\vpatki");
		driver.findElement(By.id("password")).sendKeys("August_20!7");
		driver.findElement(By.id("loginBtn")).click();

		waitForElement();
		driver.findElement(By.linkText("Categories")).click();
		driver.findElement(By.partialLinkText("Environments")).click();
		waitForElement();
		driver.findElement(By.xpath("//*[text()='v18.50']")).click();
		waitForElement();
		driver.findElement(By.xpath("//img[@alt='QAT31-45v1850']")).click();
		waitForElement();
		Thread.sleep(3000);
		driver.get("chrome://downloads/");

		Actions action = new Actions(driver);
		action.sendKeys(Keys.TAB).perform();
		action.sendKeys(Keys.TAB).perform();
		action.sendKeys(Keys.ENTER).perform();

		Thread.sleep(35000);
		Screen s = new Screen();
		try {

			// Wait for Samson login page

			s.wait("images/samson/main-login.JPG");

			// Click on User ID

			s.click("images/samson/userID.JPG");

			// Write User ID and Click TAB

			s.write("1974#TAB.");

			// Write Password Click TAB and Click ENTER

			s.write("unix31a#TAB.");

			s.write("#ENTER.");

			// Double Click on CSM

			// s.click("minimize.JPG");

			s.doubleClick("images/central_login/CSM.JPG");

			// ---------------------------------------

			// Wait for CSM Application
			
			Thread.sleep(5000);

			s.wait("images/ban/pre_ban/new-ban.JPG");

			// Click on New Ban

			s.click();

			// Wait for Ban Type Window

			Thread.sleep(3000);

			// s.wait("images/csm/ban/ok.JPG");

			// Click OK

			s.click("images/ban/pre_ban/ok.JPG");
			// s.click();
			s.wait("images/ban/pre_ban/billing_individual/billingIndividualWindow.JPG");
			s.write("#TAB.TMO#TAB.#TAB.TEST#TAB.#TAB.#ENTER.");

			// Wait For Address Window

			// Click on Number and Write

			s.write("1#TAB.NE#TAB.RAVINA DR#TAB.");

			// Click on Name and Write

			// Click on Zipcode and Write, TAB

			s.click("images/ban/pre_ban/billing_individual/zipcode.JPG");
			Thread.sleep(100);
			s.write("30346");
			//Thread.sleep(100);
			s.write("#TAB.");

			// Click Verify and Also Apply Standard

			s.click("images/ban/pre_ban/billing_individual/Verify.JPG");
			s.click("images/ban/pre_ban/billing_individual/applyStandard.JPG");

			// Wait for dealer authentication

			// Click OK

			s.write("#ENTER.");

			// Click on SSN and Write

			// s.click("images/ban/new_ban/main/main_information/ssn.JPG");
			s.write("415985351");

			// Click on LOC and Write

			s.click("images/ban/new_ban/main/main_information/loc.JPG");
			s.write("9981");

			// Click on CPNI and Write

			s.click("images/ban/new_ban/main/main_information/cpni.JPG");
			s.write("1");

			// Click on Contact 1 and Write

			s.click("images/ban/new_ban/main/phone_list/contact_1.JPG");
			s.write("0000000000");

			// Click on ID Type and Write

			s.click("images/ban/new_ban/main/individual_information/id_type.JPG");
			s.write("#DOWN.");

			// Click on ID State and Write

			s.click("images/ban/new_ban/main/individual_information/id_state.JPG");
			s.write("GA");

			// Click on ID No write

			s.click("images/ban/new_ban/main/individual_information/id_no.JPG");
			s.write("00000000000000000000000000000000");

			// Click on Expires write

			s.click("images/ban/new_ban/main/individual_information/expires.JPG");
			s.write("01102019");

			// Click on Birth Date write

			s.click("images/ban/new_ban/main/individual_information/birth_date.JPG");
			s.write("01101993");

			// Click on Employer write

			s.click("images/ban/new_ban/main/individual_information/employer.JPG");
			s.write("TEST");

			// Do Manual Credit Check

			s.click("images/csm/header/actions.JPG");
			// s.click("images/csm/header/actions/manual_credit_evaluation.JPG");
			for (int i = 0; i < 6; i++) {
				s.write("#DOWN.");
			}

			s.write("#ENTER.");
			s.write("#ENTER.");

			// Manuel Credit Check Evaluation

			// Click and Write External Ref
			s.click("images/ban/new_ban/manual_credit_evaluation/external_ref.JPG");
			s.write("EFX#TAB.");

			// Click and Write Credit Class
			s.click("images/ban/new_ban/manual_credit_evaluation/credit_class.JPG");
			s.write("A");

			// Click and Write Referral Code

			s.click("images/ban/new_ban/manual_credit_evaluation/referral_code.JPG");
			s.write("EFX#TAB.");

			// Click OK

			s.click("images/ban/new_ban/manual_credit_evaluation/ok.JPG");

			// Click close on Customer Profile
			Thread.sleep(3000);
			// s.write();
			s.type(Key.F4, Key.CTRL);

			// Click on new ban

			s.click("images/csm/new_sub.JPG");

			// Click skip on card validation

			// Move up the window
			Thread.sleep(1000);
			s.type(Key.SPACE, Key.ALT);
			s.write("#ENTER.");
			for (int i = 0; i < 7; i++) {
				s.write("#UP.");
			}
			Thread.sleep(6000);
			s.write("#ENTER.");
			Thread.sleep(1000);
			s.write("#ENTER.");
			Thread.sleep(5000);
			// End Move up the window

			s.click("images/subscriber/card_validation/skip.JPG");

			// Click GSM
			s.doubleClick("images/subscriber/line_type/gsm.JPG");
			s.write("#ENTER.");

			// Click Verify and Also Apply Standard
			Thread.sleep(5000);
			s.click("images/ban/pre_ban/billing_individual/Verify.JPG");
			s.click("images/ban/pre_ban/billing_individual/applyStandard.JPG");
			Thread.sleep(1000);
			
			//Check PPU
			
			s.click("images/subscriber/ppu/ppu.JPG");
			s.click("images/subscriber/ppu/yes.JPG");
			
			//Click Profile Tab
			
			s.click("images/subscriber/profiles/profile.JPG");
			
			//Click 911 Address
			
			s.click("images/subscriber/profiles/e911_address.JPG");
			
			
			// Wait For Address Window

			// Click on Number and Write

			s.write("1#TAB.NE#TAB.RAVINA DR#TAB.");

			// Click on Name and Write

			// Click on Zipcode and Write, TAB

			s.click("images/ban/pre_ban/billing_individual/zipcode.JPG");
			Thread.sleep(100);
			s.write("30346");
			Thread.sleep(100);
			s.write("#TAB.");
			
			//Click 911 Indicator
			s.click("images/subscriber/profiles/911_ind.JPG");
			
			// Click Verify and Also Apply Standard

			s.click("images/ban/pre_ban/billing_individual/Verify.JPG");
			s.click("images/ban/pre_ban/billing_individual/applyStandard.JPG");
			
			//Check Primary Indicator
			s.click("images/subscriber/profiles/primary_ind.JPG");
			
			//Click on device activation type
			s.wait("images/subscriber/profiles/device_activation_type.JPG");
			s.rightAt(10);
			s.click();
			
			//Write EIP
			s.write("#DOWN.#TAB.");
			
			//Click Service Equipment Tab
			s.click("images/subscriber/services_equipment/service_equipment.JPG");
			
			//Click MSISSDN
			s.doubleClick("images/subscriber/services_equipment/new_sub/msisdn.JPG");
			
			//s.write("#ENTER.#ENTER");
			
			Thread.sleep(12000);
			
			//Click Number Group and write ATG
			s.click("images/subscriber/services_equipment/new_sub/reserve_num_group/number_group.JPG");
			s.write("ATG");
			
			//Click Get and OK 
			s.click("images/subscriber/services_equipment/new_sub/reserve_num_group/get.JPG");
			s.click("images/subscriber/services_equipment/new_sub/reserve_num_group/ok.JPG");
			Thread.sleep(5000);
			//Click LOC and Write
			s.click("images/subscriber/services_equipment/new_sub/loc.JPG");
			s.write("9981");
			
			//Click on Plan and Write FRLTUNL
			s.click("images/subscriber/services_equipment/new_sub/code.JPG");
			s.write("FRLTUNL#TAB.");
			
			Thread.sleep(6000);
			s.click("images/subscriber/services_equipment/new_sub/ok.JPG");
			
			//Click on SIM and write
			s.click("images/subscriber/services_equipment/new_sub/add.JPG");

			s.click("images/subscriber/services_equipment/new_sub/add_new_gsm_equip/sim.JPG");
			s.write("8901260870014072484#TAB.#ENTER");
			
			//Click on IMEI number and Write
			s.click("images/subscriber/services_equipment/new_sub/add.JPG");
			s.click("images/subscriber/services_equipment/new_sub/add_new_gsm_equip/imei.JPG");
			s.write("359444022632555#TAB.#ENTER.");
			
			//Click Activate
			s.click("images/subscriber/buttons/activate.JPG");
			
			
			

		} catch (FindFailed e) {
			e.printStackTrace();
		}

	}

	public void waitForElement() {
		try {
			Thread.sleep(1000);
		} catch (InterruptedException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
	}

}
